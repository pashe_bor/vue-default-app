import Vue from 'vue';
import Router from 'vue-router';
import Main from './../components/Main';
import Numbers from './../components/Numbers';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    }, {
      path: '/numbers',
      name: 'Numbers',
      component: Numbers,
    },
  ],
});
